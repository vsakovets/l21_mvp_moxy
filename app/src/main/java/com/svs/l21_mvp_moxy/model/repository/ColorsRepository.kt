package com.svs.l21_mvp_moxy.model.repository

import com.svs.l21_mvp_moxy.model.ColorModel

class ColorsRepository {
fun getColorsList():List<ColorModel>{
    return listOf(
        ColorModel("#fcdb03", "yellow"),
        ColorModel("#2dfc03", "green"),
        ColorModel("#03fcf4", "blue"),
        ColorModel("#9403fc", "purple"),
        ColorModel("#fc0318", "red"),
        ColorModel("#fc9d03", "orange"),
        ColorModel("#000000", "black"),
        ColorModel("#666666", "gray")
    )
}
}