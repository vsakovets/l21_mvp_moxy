package com.svs.l21_mvp_moxy.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.svs.l21_mvp_moxy.R
import com.svs.l21_mvp_moxy.model.ColorModel
import kotlinx.android.synthetic.main.recycler_colors_model.view.*

class ColorsAdapter(private var colorsList: List<ColorModel>) : RecyclerView.Adapter<ColorsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_colors_model, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return colorsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(colorsList[position])
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val colorNameTextView = itemView.tv_color
        private val colorImageView = itemView.iv_color_value

        fun bind(colorModel: ColorModel){
            colorNameTextView.text = colorModel.colorName
            colorImageView.setBackgroundColor(Color.parseColor(colorModel.colorValue))
        }
    }

}