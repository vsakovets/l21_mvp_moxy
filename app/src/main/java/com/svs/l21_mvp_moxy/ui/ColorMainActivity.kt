package com.svs.l21_mvp_moxy.ui

import android.os.Bundle
import android.os.Message
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.svs.l21_mvp_moxy.R
import com.svs.l21_mvp_moxy.model.ColorModel
import com.svs.l21_mvp_moxy.ui.adapters.ColorsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class ColorMainActivity : MvpAppCompatActivity(), ColorsView {

    @InjectPresenter
    lateinit var colorPresenter: ColorsPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showColors(colorPresenter.getColors())


        swipeToRefresh.setOnRefreshListener {
            colorPresenter.countAmountOfSwipes()
            swipeToRefresh.isRefreshing = false
        }

    }


    override fun showColors(colorList: List<ColorModel>) {
        recyclerViewColors.layoutManager = LinearLayoutManager(this)
        recyclerViewColors.adapter = ColorsAdapter(colorPresenter.getColors())
    }

    override fun showError(message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}
