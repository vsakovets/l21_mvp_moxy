package com.svs.l21_mvp_moxy.ui


import com.svs.l21_mvp_moxy.model.ColorModel
import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface ColorsView : MvpView {
    fun showColors(colorList: List<ColorModel>)
    fun showError(message: Int)
}