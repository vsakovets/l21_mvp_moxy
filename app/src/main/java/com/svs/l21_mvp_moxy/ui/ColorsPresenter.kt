package com.svs.l21_mvp_moxy.ui

import android.provider.Settings.Global.getString
import com.svs.l21_mvp_moxy.R
import com.svs.l21_mvp_moxy.model.ColorModel
import com.svs.l21_mvp_moxy.model.repository.ColorsRepository
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class ColorsPresenter : MvpPresenter<ColorsView>() {
    private var colorsRepository: ColorsRepository = ColorsRepository()

     private var counter = 0
    fun countAmountOfSwipes() {
        counter++
        if (counter == 3) {
            viewState.showError(R.string.error_text)
            counter = 0
        }
    }

    fun getColors() :List<ColorModel> {
        return colorsRepository.getColorsList()
    }
}